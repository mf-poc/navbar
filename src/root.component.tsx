export interface Props {
  name: string;
}

export default function Root(props: Props) {
  return <section>{props.name} is mounted!</section>;
}
